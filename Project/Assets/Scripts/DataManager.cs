﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManager : MonoBehaviour {

    public static DataManager dataManagement;

    public int highScore;

    void Awake()
    {
        if(dataManagement == null)
        {
            DontDestroyOnLoad(gameObject);
            dataManagement = this;
        }else if (dataManagement != this)
        {
            Destroy(gameObject);
        }
    }

    public void SaveData()
    {
        BinaryFormatter binForm = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gameinfo.dat"); //létrehozza a fájlt 
        GameData data = new GameData();
        data.highscore = highScore; //data.highscore-ba(elmentett fájl) menti a jelenlegi highscore-t
        binForm.Serialize(file, data); //titkosítja a fájlt, csalás ellen
        file.Close();
    }
    public void LoadData()
    {
        if(File.Exists(Application.persistentDataPath + "/gameinfo.dat"))
        {
            BinaryFormatter BinForm = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gameinfo.dat", FileMode.Open);
            GameData data = (GameData)BinForm.Deserialize(file);     
            file.Close();
            highScore = data.highscore;
        }
    }
}
[Serializable]
class GameData //ez tárolja a menteni kívánt adatokat, később bővül
{
    public int highscore;
}
