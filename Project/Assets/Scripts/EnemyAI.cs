﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyAI : MonoBehaviour
{
    public int enemySpeed = 2;
    public int xDirection = 1;
    public float enemyWidth = 0.3f;
    public DeathMenu deathMenu;
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(xDirection, 0));
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(xDirection, 0) * enemySpeed;
        if (hit.distance < enemyWidth)
        {
            flip();
            if (hit.collider.name == "player")
            {
                FindObjectOfType<AudioManager>().PlaySound("PlayerKilled");
                Destroy(hit.collider.gameObject); // Játékos halál
                deathMenu.ToggleDeathMenu();  //HA ÚJ SCENE ERROR - ENEMYRE IS HÚZD RÁ A DEATHMENUT
            }
        }
    }
    void flip()
    {
        if(xDirection > 0)
        {
            xDirection = -1;
            gameObject.GetComponent<SpriteRenderer>().flipX = false; // karakter forgatása
        }
        else
        {
            xDirection = 1;
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }     
    }
}
