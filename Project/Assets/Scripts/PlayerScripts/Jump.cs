﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{

    [Range(1, 10)]
    public float jumpVelocity;
    public float playerBottom = 0.3f;
    public LayerMask mask;

    bool jumpReq;
    bool grounded;

    Vector2 playerSize;
    Vector2 boxSize;

    void Awake()
    {
        playerSize = GetComponent<BoxCollider2D>().size;
        boxSize = new Vector2(playerSize.x, playerBottom);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && grounded)
        {
            jumpReq = true;
        }
    }
    void FixedUpdate()
    {
        if (jumpReq)
        {
            //GetComponent<Rigidbody2D>().velocity += Vector2.up * jumpVelocity;
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpVelocity, ForceMode2D.Impulse);

            jumpReq = false;
            grounded = false;
        }else
        {
            Vector2 boxCenter = (Vector2)transform.position + Vector2.down * (playerSize.y + boxSize.y) * 0.5f;
            grounded = (Physics2D.OverlapBox(boxCenter, boxSize, 0f, mask) != null);
        }
    }
}
