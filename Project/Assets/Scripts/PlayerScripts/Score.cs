﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    private float timeLeft = 120;
    public int score = 0;
    public int pickedCoins = 0;
    public GameObject timeLeftUI;
    public GameObject scoreUI;
    public GameObject coinsUI;
    public GameObject currentLevelUI;
    public DeathMenu death;
    public EndLevel endScreen;

    public int currentLevel;
    public int levelToUnlock;

    private void Start()
    {
        DataManager.dataManagement.LoadData();
        currentLevel = SceneManager.GetActiveScene().buildIndex;
        levelToUnlock = currentLevel + 1;
    }
    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        timeLeftUI.gameObject.GetComponent<TextMeshProUGUI>().text = ("Time left: " + (int)timeLeft);
        scoreUI.gameObject.GetComponent<TextMeshProUGUI>().text = ("Score: " + score);
        coinsUI.gameObject.GetComponent<TextMeshProUGUI>().text = ("Coins: " + pickedCoins);
        currentLevelUI.gameObject.GetComponent<TextMeshProUGUI>().text = ("" + SceneManager.GetActiveScene().name);
        if (timeLeft < 0.1f) //Idő lejár
        {
            death.ToggleDeathMenu();
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D trigger)
    {
        if (trigger.gameObject.tag == "endlevel") //Pálya vége
        {
            countScore();
            endScreen.ToggleEndScreen();
            gameObject.GetComponent<PlayerControl>().playerSpeed = 0;
            PlayerPrefs.SetInt("levelReached", levelToUnlock);
        }
        if (trigger.gameObject.tag == "coin") //Coin felvétel
        {
            FindObjectOfType<AudioManager>().PlaySound("CoinPickup");
            score += 20;
            pickedCoins++;
            Destroy(trigger.gameObject);
        }
    }

    void countScore() //Pálya végén hívódik meg
    {
        score = score + (int)(timeLeft * 10);
        DataManager.dataManagement.highScore += score;
        HighScores.AddNewHighscore(PlayerPrefs.GetString("Username"), DataManager.dataManagement.highScore);
        DataManager.dataManagement.SaveData();
    }
}
