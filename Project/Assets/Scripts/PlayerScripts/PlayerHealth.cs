﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public DeathMenu deathMenu;

    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.y < -7)
        {
            FindObjectOfType<AudioManager>().PlaySound("PlayerFallDeath");
            Destroy(gameObject); // Játékos halál
            deathMenu.ToggleDeathMenu();
        }
    }
}
