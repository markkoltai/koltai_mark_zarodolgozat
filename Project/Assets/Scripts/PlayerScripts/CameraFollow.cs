﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    private GameObject player;
    public float xMin = -50; //kamera bal széle amíg mozoghat az x tengelyen (pálya eleje)
    public float xMax = 100; //kamera jobb széle amíg mozoghat az x tengelyen (pálya vége)
    public float yMin = 0;
    public float yMax = 15;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("player"))
        {
            float x = Mathf.Clamp(player.transform.position.x, xMin, xMax);
            float y = Mathf.Clamp(player.transform.position.y, yMin, yMax);
            gameObject.transform.position = new Vector3(x, y, gameObject.transform.position.z);
        }
        else if (GameObject.Find("player") == null)
        {
            Camera.main.GetComponent<CameraFollow>().enabled = false;
        }

    }
}
