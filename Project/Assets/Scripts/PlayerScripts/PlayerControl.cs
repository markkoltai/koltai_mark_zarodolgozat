﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public int playerSpeed = 5;
    //public bool facingLeft = false;
    public float moveX;
    public bool isGrounded;
    public float playerBottom = 0.3f; //távolság a karakter közepétől az aljáig

    private void Start()
    {
        FindObjectOfType<AudioManager>().PlaySound("Ambient");
        Cursor.visible = false;
    }
    // Update is called once per frame
    void Update()
    {
        movePlayer();
        playerRaycast();
    }
    void movePlayer()
    {
        //Irányítás
        moveX = Input.GetAxis("Horizontal");
        //Animáció
        if (moveX != 0)
        {
            GetComponent<Animator>().SetBool("isRunning", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("isRunning", false);
        }
        //Játékos irány
        if (moveX < 0.0f)
        {
            //flipPlayer();
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (moveX > 0.0f)
        {
            //flipPlayer();
            GetComponent<SpriteRenderer>().flipX = false;
        }
        //Fizika
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }
    void playerRaycast()
    {
        RaycastHit2D rayDown = Physics2D.Raycast(transform.position, Vector2.down);
        if (rayDown.collider != null && rayDown.distance < playerBottom && rayDown.collider.tag == "enemy")
        {       
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 200);
            GetComponent<Score>().score += 50;
            FindObjectOfType<AudioManager>().PlaySound("MobDeath");
            Destroy(rayDown.collider.gameObject); // Enemy meghal
        }
        if (rayDown.collider != null && rayDown.distance < playerBottom && rayDown.collider.tag != "enemy")
        {
            isGrounded = true;
        }
    }
}