﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighScoreMenu : MonoBehaviour
{

    public TMP_Text[] highscoreText;
    HighScores highscoremanager;
    public GameObject currNameText;
    public GameObject currScoreText;

    // Use this for initialization
    void Update()
    {
        currNameText.GetComponent<TextMeshProUGUI>().text = "Current name: " + PlayerPrefs.GetString("Username");
    }
    void Start()
    {
        Cursor.visible = true;
        for (int i = 0; i < highscoreText.Length; i++)
        {
            highscoreText[i].GetComponent<TextMeshProUGUI>().text = i + 1 + ". Fetching scores...";
        }
        highscoremanager = GetComponent<HighScores>();
        StartCoroutine("RefreshHighscores");
        DataManager.dataManagement.LoadData();        
        currScoreText.GetComponent<TextMeshProUGUI>().text = "Current score: " + DataManager.dataManagement.highScore;
    }
    public void OnHighscoresDownloaded(Highscore[] highscorelist)
    {
        for (int i = 0; i < highscoreText.Length; i++)
        {
            highscoreText[i].GetComponent<TextMeshProUGUI>().text = i + 1 + ". ";
            if (highscorelist.Length > i)
            {
                highscoreText[i].GetComponent<TextMeshProUGUI>().text += highscorelist[i].username + ": " + highscorelist[i].score;
            }
        }
    }
    IEnumerator RefreshHighscores()
    {
        while (true)
        {
            highscoremanager.DownloadHighscores();
            yield return new WaitForSeconds(30);
        }
    }
}
