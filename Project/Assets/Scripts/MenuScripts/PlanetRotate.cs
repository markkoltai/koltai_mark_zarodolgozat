﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotate : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        // Rotate the object around its local X axis at 1 degree per second
        // A bolygót a saját x tengelye körül forgatja, 3 fok/mp-ként
        transform.Rotate(Vector3.right * Time.deltaTime*3);

        // ...also rotate around the World's Y axis
        // A világ y tengelye körül is forgatja
        transform.Rotate(Vector3.up * Time.deltaTime*3, Space.World);
    }
}
