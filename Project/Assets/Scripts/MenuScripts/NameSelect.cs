﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NameSelect : MonoBehaviour
{
    public TMP_InputField usernameInputField;
    public TMP_InputField changenameInputField;
    public GameObject setNameMenu;
    public GameObject warningMsg;
    public GameObject warningMsg2;

    // Use this for initialization
    void Start()
    {
        Cursor.visible = true;
        if (!PlayerPrefs.HasKey("Username"))
        {
            setNameMenu.SetActive(true);
        }
        else
        {
            changenameInputField.text = PlayerPrefs.GetString("Username");
        }
    }

    public void SetUsername()
    {
        if (usernameInputField.text != " " && usernameInputField.text.Length >= 3 && usernameInputField.text.Length <= 9)
        {
            PlayerPrefs.SetString("Username", usernameInputField.text);
            Debug.Log(PlayerPrefs.GetString("Username"));
            setNameMenu.SetActive(false);
            changenameInputField.text = PlayerPrefs.GetString("Username");
            warningMsg.SetActive(true);
        }
        else
        {
            warningMsg.SetActive(true);
            Debug.Log("Rossz név");
        }
    }
    public void ChangeUsername()
    {
        if (changenameInputField.text != " " && changenameInputField.text.Length >= 3 && usernameInputField.text.Length <= 9)
        {
            PlayerPrefs.SetString("Username", changenameInputField.text);
            DataManager.dataManagement.highScore = 0;
            DataManager.dataManagement.SaveData();
            warningMsg2.SetActive(false);
        }
        else
        {
            warningMsg2.SetActive(true);
        }
    }
}
