﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScroll : MonoBehaviour {

    public Transform[] backgrounds;   //parallaxolni kívánt elemek tömbje
    public float smoothing = 1f;      //parallax effect simasága

    private float[] parallaxScales;   //mennyit kell mozdulnia a kamerának, hogy mozduljon a háttér
    private Transform cam;            //main kamera referencia
    private Vector3 previousCamPos;   //előző képkocka kameraállása

    void Awake()
    {
        cam = Camera.main.transform;  
    }
    // Use this for initialization
    void Start () {
        previousCamPos = cam.position;

        parallaxScales = new float[backgrounds.Length];
        for (int i = 0; i < backgrounds.Length; i++)
        {
            parallaxScales[i] = backgrounds[i].position.z * -1;
        }
	}
	// Update is called once per frame
	void Update () {
        //minden háttérelem
        for (int i = 0; i < backgrounds.Length; i++)
        {
            // parallax hatás számítása
            float parallax = (previousCamPos.x - cam.position.x) * parallaxScales[i];
            float backgroundTargetPosX = backgrounds[i].position.x + parallax;
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
        }
        previousCamPos = cam.position;
	}
}
