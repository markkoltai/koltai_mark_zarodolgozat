﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]

public class Tiling : MonoBehaviour
{
    public int offsetX = 2;             //kamera szélét növeli, elöbb generál majd elemet minthogy odaérne a kamera
    public bool hasARightTile = false;  //ellenőrzi, hogy van-e szomszédja az elemnek (jobb)
    public bool hasALeftTile = false;   //ellenőrzi, hogy van-e szomszédja az elemnek (bal)
    public bool reverseScale = false;   //ha esetleg nem használható tileként az elem.

    private float spriteWidth = 0f;     //elem szélessége
    private Camera cam;                 //camera referencia
    private Transform myTransform;      //transform referencia

    void Awake()
    {
        cam = Camera.main;
        myTransform = transform;
    }
    // Use this for initialization
    void Start()
    {
        SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
        spriteWidth = sRenderer.sprite.bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasARightTile == false || hasALeftTile == false)
        {
            //kiszámolja a távot a kamera közelpétől a széléig, koordinátában
            float camHorizontalExtend = cam.orthographicSize * Screen.width / Screen.height;
            //kiszámolja a pozíciót, ahol a kamera látja az elem szélét
            float edgeVisiblePosRight = (myTransform.position.x + spriteWidth / 2) - camHorizontalExtend;
            float edgeVisiblePosLeft = (myTransform.position.x - spriteWidth / 2) + camHorizontalExtend;

            if (cam.transform.position.x >= edgeVisiblePosRight - offsetX && hasARightTile == false)
            {
                MakeNewTile(1);
                hasARightTile = true;
            }
            else if (cam.transform.position.x <= edgeVisiblePosLeft + offsetX && hasALeftTile == false)
            {
                MakeNewTile(-1);
                hasALeftTile = true;
            }
        }
    }
    void MakeNewTile(int direction)
    {
        //az új elem pozícióját számolja
        Vector3 newPos = new Vector3(myTransform.position.x + spriteWidth * direction, myTransform.position.y, myTransform.position.z);
        //létrehozza az új elemet
        Transform newTile = Instantiate(myTransform, newPos, myTransform.rotation);

        if (reverseScale == true)
        {
            newTile.localScale = new Vector3(newTile.localScale.x * -1, newTile.localScale.y, newTile.localScale.z);
        }
        newTile.parent = myTransform.parent;
        if (direction < 0)
        {
            newTile.GetComponent<Tiling>().hasARightTile = true;
        }
        if (direction > 0)
        {
            newTile.GetComponent<Tiling>().hasALeftTile = true;
        }
    }
}